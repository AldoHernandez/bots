from datetime import datetime
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import pandas as pd
import time
import datetime 
import csv
import os.path
from datetime import date
from datetime import datetime
import shutil


def BOT_MORELOS():
    datos=pd.read_csv('csv\MORELOS_DATA.csv',header=0)

    today = str(datetime.today().strftime('%Y-%m-%d'))
    MORELOS_Adeudos = 'MORELOS_Adeudos-' + today

    print(MORELOS_Adeudos)

    if os.path.exists(MORELOS_Adeudos+'.csv'):
        print("File Exists")

        datos_leer=pd.read_csv('data.csv',header=0)

        datos_escribir=pd.read_csv(MORELOS_Adeudos+'.csv',na_filter= False,header=0)

        print(len(datos_leer))
        print(len(datos_escribir))

        print(datos_leer['PLACA'][len(datos_leer)-1])
        print(datos_escribir['PLACA'][len(datos_escribir)-1])
        #print(datos_leer['PLACA'][len(datos_escribir)-1])

        last_registro = len(datos_escribir)-1

        if len(datos_leer) > len(datos_escribir) :

            options = webdriver.FirefoxOptions()
            options.add_argument('--start-maximized')
            options.add_argument('--disabled-extensions')

            driver = webdriver.Firefox(executable_path=r"C:\dfirefox\geckodriver.exe", firefox_options=options)
            driver.get("https://www.hacienda.morelos.gob.mx/")

            driver.set_window_position(2000,0)
            driver.maximize_window()

            #Arrays to save data
            fila = []
            mylist = ['','','','','','','']

            for x in range(0,len(datos)):
            
                buttonpagos = driver.find_element_by_xpath('/html/body/div[1]/div/div/main/div[5]/div/button')
                time.sleep(2)
                buttonpagos.click()

                WebDriverWait(driver, 5)\
                    .until(EC.element_to_be_clickable((By.CSS_SELECTOR,
                                                    'div.container1'.replace(' ', '.'))))\
                    .click()

                placa = str(datos['PLACA'][x])
                vin = str(datos['VIN'][x])

                print(placa)
                
                value = 0

                #while value < 1 :

                time.sleep(1)

                buttonmodal = driver.find_element_by_xpath('/html/body/div[1]/div/div/main/div[6]/div[2]/div[1]/div[2]/button[1]')
                
                buttonmodal.click()
                
                time.sleep(2)

                opcion = driver.find_element_by_xpath('//*[@id="lblOficinaDocumento"]').get_attribute('style')
                print (opcion)

                if opcion != "display: none;" :
                    WebDriverWait(driver, 10)\
                        .until(EC.element_to_be_clickable((By.XPATH,
                                                        '//*[@id="lblOficinaDocumento"]')))\
                        .click()
                    
                    value = 1
                else :

                    WebDriverWait(driver, 5)\
                    .until(EC.element_to_be_clickable((By.XPATH,
                                                    '//*[@id="modalFormulario-cancelar"]')))\
                    .click()
                    
                    value = 0
                
                time.sleep(1)
                    

                WebDriverWait(driver, 2)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '//*[@id="lblOficinaDocumento"]')))\
                .click()
                    
                WebDriverWait(driver, 2)\
                    .until(EC.element_to_be_clickable((By.XPATH,
                                                    '/html/body/div[6]/div/div/div/div[2]/div/div[6]/select/option[2]')))\
                    .click()

                WebDriverWait(driver, 1)\
                    .until(EC.element_to_be_clickable((By.XPATH,
                                                    '//*[@id="placa"]')))\
                    .send_keys(str(placa.strip()))

                WebDriverWait(driver, 1)\
                    .until(EC.element_to_be_clickable((By.XPATH,
                                                    '//*[@id="numeroSerieCorto"]')))\
                    .send_keys(str(vin.strip()))

                WebDriverWait(driver, 2)\
                    .until(EC.element_to_be_clickable((By.XPATH,
                                                    '//*[@id="modalFormulario-continuar"]')))\
                    .click()

                if driver.find_element_by_xpath('/html/body/div[6]/div/div/div/div[3]/table'):
                
                    texto_tabla = driver.find_element_by_xpath('/html/body/div[6]/div/div/div/div[3]/table')
                    rows= texto_tabla.find_elements(By.TAG_NAME, 'tr')
                    
                    #contador filas
                    cont_row = 1

                    for r in rows:
                    
                        cols = r.find_elements(By.TAG_NAME, 'td')
                        for c in cols:
                            fila.append(c.text)

                        slist = list(filter(None, fila))

                        #print(slist)
                    
                        print(len(fila))
                        print('Fila')
                        print(fila)

                        if cont_row > 1 :
                                mylist[0] = datos['PLACA'][x]
                                mylist[1] = fila[1]
                                mylist[2] = fila[2]
                                mylist[3] = fila[3]
                                mylist[4] = fila[4]
                                mylist[5] = fila[5]
                                mylist[6] = str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))
                                fila.append(str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S')))
                                
                                #Here append in csv
                                
                                if mylist[2] != '' :
                                    
                                    with open(MORELOS_Adeudos+'.csv', "a", newline='') as fp:
                                        wr = csv.writer(fp, dialect='excel')
                                        wr.writerow(mylist)

                        fila = []
                        cont_row = cont_row + 1
                
                    time.sleep(1)


                if driver.find_element_by_xpath('/html/body/div[6]/div/div/div/div[1]/center[2]'):
                
                    texto_mensaje = driver.find_element_by_xpath('/html/body/div[6]/div/div/div/div[1]/center[2]')
                    texto_mensaje = texto_mensaje.text

                    time.sleep(1)
                
                    if texto_mensaje != "":
                        fila = [datos['PLACA'][x],texto_mensaje,"","","",""]
                        mylist = [datos['PLACA'][x],texto_mensaje,"","","",str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))] 
                        fila = []

                        with open(MORELOS_Adeudos+'.csv', "a", newline='') as fp:
                            wr = csv.writer(fp, dialect='excel')
                            wr.writerow(mylist)
                        
                WebDriverWait(driver, 3)\
                    .until(EC.element_to_be_clickable((By.XPATH,
                                                    '//*[@id="modalFormulario-cancelar"]')))\
                    .click()

                time.sleep(1)

                print(mylist)

                mylist = ['','','','','','','']

            shutil.move(MORELOS_Adeudos+'.csv', '../Move-here/'+MORELOS_Adeudos+'.csv')

            driver.close()

        elif len(datos_leer) <= len(datos_escribir) :
            print("Here")
            shutil.move(MORELOS_Adeudos+'.csv', '../Move-here/'+MORELOS_Adeudos+'.csv')

    else :

        with open(MORELOS_Adeudos+'.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(['PLACA','DESCRIPCION','EJERCICIO','COSTO UNITARIO','CANTIDAD','SUBTOTAL','TOTAL','FECHA'])

        
        options = webdriver.FirefoxOptions()
        options.add_argument('--start-maximized')
        options.add_argument('--disabled-extensions')

        driver = webdriver.Firefox(executable_path=r"C:\dfirefox\geckodriver.exe", firefox_options=options)
        driver.get("https://www.hacienda.morelos.gob.mx/")

        driver.set_window_position(2000,0)
        driver.maximize_window()

        #Arrays to save data
        fila = []
        mylist = ['','','','','','','']

        for x in range(0,len(datos)):

            print("Registro: " + str(x + 1) + " de " +str(len(datos)))
        
            buttonpagos = driver.find_element_by_xpath('/html/body/div[1]/div/div/main/div[5]/div/button')
            time.sleep(2)
            buttonpagos.click()

            WebDriverWait(driver, 5)\
                .until(EC.element_to_be_clickable((By.CSS_SELECTOR,
                                                'div.container1'.replace(' ', '.'))))\
                .click()

            placa = str(datos['PLACA'][x])
            vin = str(datos['VIN'][x])
            vin = vin[len(vin)-5:len(vin)]
            
            print("Placa de consulta")
            print(placa)

            print("VIN de consulta")
            print(vin)

            value = 0

            #while value < 1 :

            time.sleep(1)

            buttonmodal = driver.find_element_by_xpath('/html/body/div[1]/div/div/main/div[6]/div[2]/div[1]/div[2]/button[1]')
            
            buttonmodal.click()
            
            time.sleep(2)

            opcion = driver.find_element_by_xpath('//*[@id="lblOficinaDocumento"]').get_attribute('style')
            #print (opcion)

            if opcion != "display: none;" :
                WebDriverWait(driver, 10)\
                    .until(EC.element_to_be_clickable((By.XPATH,
                                                    '//*[@id="lblOficinaDocumento"]')))\
                    .click()
                
                value = 1
            else :

                WebDriverWait(driver, 5)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '//*[@id="modalFormulario-cancelar"]')))\
                .click()
                
                value = 0
            
            time.sleep(1)
                

            WebDriverWait(driver, 2)\
            .until(EC.element_to_be_clickable((By.XPATH,
                                            '//*[@id="lblOficinaDocumento"]')))\
            .click()
                
            WebDriverWait(driver, 2)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '/html/body/div[6]/div/div/div/div[2]/div/div[6]/select/option[2]')))\
                .click()

            WebDriverWait(driver, 1)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '//*[@id="placa"]')))\
                .send_keys(str(placa.strip()))

            WebDriverWait(driver, 1)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '//*[@id="numeroSerieCorto"]')))\
                .send_keys(str(vin.strip()))

            WebDriverWait(driver, 2)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '//*[@id="modalFormulario-continuar"]')))\
                .click()

            if driver.find_element_by_xpath('/html/body/div[6]/div/div/div/div[3]/table'):
            
                texto_tabla = driver.find_element_by_xpath('/html/body/div[6]/div/div/div/div[3]/table')
                rows= texto_tabla.find_elements(By.TAG_NAME, 'tr')
                
                #contador filas
                cont_row = 1

                for r in rows:
                
                    cols = r.find_elements(By.TAG_NAME, 'td')
                    for c in cols:
                        fila.append(c.text)

                    slist = list(filter(None, fila))

                    #print(slist)
                
                    print(len(fila))
                    print('Fila')
                    print(fila)

                    if cont_row > 1 :
                            mylist[0] = placa
                            mylist[1] = fila[1]
                            mylist[2] = fila[2]
                            mylist[3] = fila[3]
                            mylist[4] = fila[4]
                            mylist[5] = fila[5]
                            mylist[6] = ""
                            mylist.append(str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S')))
                            fila.append(str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S')))
                            
                            #//*[@id="totalPagar"]
                            #Here append in csv
                            total = driver.find_element_by_xpath('//*[@id="totalPagar"]')
                            mylist[6] =  total.text
                            
                            if mylist[2] != '' :
                                
                                with open(MORELOS_Adeudos+'.csv', "a", newline='') as fp:
                                    wr = csv.writer(fp, dialect='excel')
                                    wr.writerow(mylist)

                    fila = []
                    cont_row = cont_row + 1
            
                time.sleep(1)


            if driver.find_element_by_xpath('/html/body/div[6]/div/div/div/div[1]/center[2]'):
            
                texto_mensaje = driver.find_element_by_xpath('/html/body/div[6]/div/div/div/div[1]/center[2]')
                texto_mensaje = texto_mensaje.text

                time.sleep(1)
            
                if texto_mensaje != "":
                    fila = [datos['PLACA'][x],texto_mensaje,"","","","",""]
                    mylist = [datos['PLACA'][x],texto_mensaje,"","","","",str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))] 
                    fila = []

                    with open(MORELOS_Adeudos+'.csv', "a", newline='') as fp:
                        wr = csv.writer(fp, dialect='excel')
                        wr.writerow(mylist)
                    
            WebDriverWait(driver, 3)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '//*[@id="modalFormulario-cancelar"]')))\
                .click()

            time.sleep(1)

            print(mylist)

            mylist = ['','','','','','','']

        
        #shutil.move(MORELOS_Adeudos+'.csv', '../Move-here/'+MORELOS_Adeudos+'.csv')

        driver.close()
