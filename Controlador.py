
#Import Functions BOTS
from CDMX import BOT_CDMX
from MORELOS import BOT_MORELOS
from PUEBLA import BOT_PUEBLA
from REPUVE import BOT_REPUVE
from RAPPI import BOT_RAPPI
#from EDOMEX import BOT_EDOMEX
from CDMX_ADEUDOS import BOT_CDMX_ADEUDOS
from TAMAULIPAS import BOT_TAMAULIPAS

print("Ingrese el numero correspondiente al Bot que desea ejecutar:")
print("1 ----> LISTAS REPUVE/RAPPI")
print("2 ----> BOTS ESTADOS")
numero = int(input("Digite un número: "))


if numero == 1 :
    print("REPUVE/RAPPI")
    print("Ingrese el número correspondiente al Bot que desea ejecutar:")
    print("1 ----> REPUVE")
    print("2 ----> BOTS ESTADOS")
    opcion = int(input("Digite el número de la opción que desea realizar..."))

    if opcion == 1 :
        BOT_REPUVE()
    elif opcion == 2 :
        BOT_RAPPI()
elif numero == 2 :
    print("BOTS")
    print("Ingrese el número correspondiente al Bot que desea ejecutar:")
    print("1 ----> CDMX ADEUDOS")
    print("2 ----> CDMX")
    print("3 ----> EDOMEX")
    print("4 ----> MORELOS")
    print("5 ----> PUEBLA")
    print("6 ----> TAMAULIPAS")
        
    opcion = int(input("Digite el número de la opción que desea realizar..."))

    if opcion == 1 :
        BOT_CDMX_ADEUDOS()
    elif opcion == 2:
        BOT_CDMX()
    elif opcion == 3:
        #BOT_EDOMEX()
        print('...')
    elif opcion == 4:
        BOT_MORELOS()
    elif opcion == 5:
        BOT_PUEBLA()
    elif opcion == 6:
        BOT_TAMAULIPAS()
    else :
        print("OPCION INCORRECTA")

