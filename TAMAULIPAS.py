from datetime import datetime
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import pandas as pd
import time
import datetime 
import csv
import os.path
import shutil
from datetime import date
from datetime import datetime


def BOT_TAMAULIPAS():

    today = str(datetime.today().strftime('%Y-%m-%d'))
    Tamaulipas_Adeudos = 'Tamaulipas_Adeudos-' + today


    options = webdriver.FirefoxOptions()
    options.add_argument('--start-maximized')
    options.add_argument('--disabled-extensions')

    driver = webdriver.Firefox(executable_path=r"C:\dfirefox\geckodriver.exe", firefox_options=options)
    driver.get("https://finanzas.tamaulipas.gob.mx/pago-de-contribuciones/tenencia.php")

    driver.set_window_position(2000,0)
    driver.maximize_window()
    time.sleep(3)

    adeudos = []
    mensajes = []
    placas = []
    fecha = []

    list = ['','','','','','']


    with open(Tamaulipas_Adeudos+'.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["PLACA", "ADEUDOS", "DESCRIPCION", "IMPORTE", "ACTUALIZACION","RECARGOS","TOTAL", "FECHA"])

    #datos=pd.read_csv('Tamaulipas.csv',header=0)


    #for x in range(0,len(datos)):

    placa = 'WE6636A'
    vin = '00060'
    municipio1 = 'VICTORIA'
    municipio2 = 'REYNOSA'
    municipio3 = 'NUEVO LAREDO'
    municipio4 = 'MANTE'
    municipio5 = 'MATAMOROS'
   
    opcion1 = '/html/body/form/table/tbody/tr[1]/td/div[1]/table/tbody/tr[2]/td[2]/select/option[41]'
    opcion2 = '/html/body/form/table/tbody/tr[1]/td/div[1]/table/tbody/tr[2]/td[2]/select/option[32]'
    opcion3 = '/html/body/form/table/tbody/tr[1]/td/div[1]/table/tbody/tr[2]/td[2]/select/option[27]'
    opcion4 = '/html/body/form/table/tbody/tr[1]/td/div[1]/table/tbody/tr[2]/td[2]/select/option[21]'
    opcion5 = '/html/body/form/table/tbody/tr[1]/td/div[1]/table/tbody/tr[2]/td[2]/select/option[22]'
    #placa = "413XMG"

#     WebDriverWait(driver, 5)\
#        .until(EC.element_to_be_clickable((By.XPATH,'/html/body/form/table/tbody/tr/td/p[1]/input[1]')))\
#        .click()
 
    time.sleep(5)

    frame = driver.find_element_by_xpath('//*[@id="ifrm"]')
    driver.switch_to.frame(frame)

    driver.find_element_by_css_selector("body > form:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > p:nth-child(2) > input:nth-child(1)").click()

    WebDriverWait(driver, 2)\
        .until(EC.element_to_be_clickable((By.XPATH,'/html/body/form/table/tbody/tr[1]/td/div[1]/table/tbody/tr[3]/td[2]/input')))\
        .send_keys(placa.strip())

    WebDriverWait(driver, 2)\
        .until(EC.element_to_be_clickable((By.XPATH,'/html/body/form/table/tbody/tr[1]/td/div[1]/table/tbody/tr[4]/td[2]/input')))\
        .send_keys(vin)


    #print (placa)

    
    WebDriverWait(driver, 2)\
        .until(EC.element_to_be_clickable((By.XPATH,'//*[@id="menu_municipios"]')))\
        .click()

    WebDriverWait(driver, 2)\
        .until(EC.element_to_be_clickable((By.XPATH,opcion3)))\
        .click()

    

    WebDriverWait(driver, 2)\
        .until(EC.element_to_be_clickable((By.XPATH,
        '/html/body/form/table/tbody/tr[1]/td/div[1]/table/tbody/tr[5]/td/input[2]')))\
        .click()
    
    time.sleep(25)



    #driver.find_element_by_xpath('//*[@id="inputPlaca"]').clear()
    #driver.find_element_by_xpath('//*[@id="inputPlaca"]').clear()

    #driver.close()

BOT_TAMAULIPAS()