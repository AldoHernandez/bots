from datetime import datetime
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import pandas as pd
import time
import datetime 
import csv
import os.path
import shutil
from datetime import date
from datetime import datetime


def BOT_CDMX():

    today = str(datetime.today().strftime('%Y-%m-%d'))
    CDMX_Adeudos = 'CDMX_Adeudos-' + today


    options = webdriver.FirefoxOptions()
    options.add_argument('--start-maximized')
    options.add_argument('--disabled-extensions')

    driver = webdriver.Firefox(executable_path=r"C:\dfirefox\geckodriver.exe", firefox_options=options)
    driver.get("https://data.finanzas.cdmx.gob.mx/consultas_pagos/consulta_adeudosten")

    driver.set_window_position(2000,0)
    driver.maximize_window()
    time.sleep(3)

    adeudos = []
    mensajes = []
    placas = []
    fecha = []

    list = ['','','','','','']


    with open(CDMX_Adeudos+'.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["PLACA", "ADEUDOS", "MENSAJES", "PLACA ANTERIOR", "OBSERVACIONES", "FECHA"])

    datos=pd.read_csv('csv\\CDMX.csv',header=0)


    for x in range(0,len(datos)):

        placa = str(datos['Placa'][x])

        #placa = "413XMG"

        WebDriverWait(driver, 2)\
            .until(EC.element_to_be_clickable((By.XPATH,'//*[@id="inputPlaca"]')))\
            .send_keys(placa.strip())

        

        print (placa)

        #time.sleep(2)

        WebDriverWait(driver, 5)\
            .until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div[2]/div[1]/div/div[3]/div/div/div/div/div[2]/div/div/div/div/div/form/div[2]/div/button')))\
            .click()

        time.sleep(5)

        text_alert = driver.find_element_by_xpath("/html/body/div[2]/div[1]/div/div[3]/div/div/div/div/div[2]/div/div/div/div/div/form/div[1]/div/div[2]/div/div[1]")
        #/html/body/div[2]/div[1]/div/div[3]/div/div/div/div/div[2]/div/div/div/div/div/form/div[1]/div/div[2]/div/div[1]

        #text_verificar = driver.find_element_by_xpath("/html/body/div[2]/div[1]/div/div[3]/div/div/div/div/div[2]/div/div/div/div/div/form/div[1]/div/div[2]/div/div[1]")
        #/html/body/div[2]/div[1]/div/div[3]/div/div/div/div/div[2]/div/div/div/div/div/form/div[1]/div/div[2]/div/div[1]

        print(text_alert.text)


        #text_list = driver.find_element_by_id('ulAdeudos')
        text_list = driver.find_element_by_class_name('kt-widget2').find_elements_by_tag_name('div')

        print(len(text_list))

        data = []
        data2 = []

        for item in text_list:
            text = item.text    
            data.append(text.split("\n"))
            #print(text.split("\n"))


        for x in range(0,len(data)):

            if len(data[x]) > 1 :
                data2.append(data[x])


        result = []
        for item in data2:
            if item not in result:
                result.append(item)
                
        print(result)

        list = ['','','','','','']

        if len(text_list) == 0 :
            list[0] = placa
            list[1] = ''
            list[2] = ''
            list[3] = ''
            list[4] = text_alert.text
            list[5] = str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))

            with open(CDMX_Adeudos+'.csv', "a", newline='') as fp:
                        wr = csv.writer(fp, dialect='excel')
                        wr.writerow(list)

            list = ['','','','','','']
        else :

            for item in result :
                list[0] = placa
                list[1] = item[0]
                list[2] = item[1]
                list[3] = ''
                list[4] = text_alert.text
                list[5] = str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))

                with open(CDMX_Adeudos+'.csv', "a", newline='') as fp:
                            wr = csv.writer(fp, dialect='excel')
                            wr.writerow(list)

                list = ['','','','','','']
        #text_alert.text = ''
 
        #writer.writerow(["PLACA", "ADEUDOS", "MENSAJES", "PLACA ANTERIOR", "OBSERVACIONES", "FECHA"])

        driver.find_element_by_xpath('//*[@id="inputPlaca"]').clear()

    
    driver.close()
