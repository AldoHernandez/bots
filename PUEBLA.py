from ast import Or
from datetime import datetime
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import pandas as pd
import time
import datetime 
import csv
import os.path
from datetime import date
from datetime import datetime
import shutil
import base64
import cv2
import pytesseract
from PIL import Image
from selenium.common.exceptions import NoSuchElementException
from os import remove
from os import path


options = webdriver.FirefoxOptions()
options.add_argument('--start-maximized')
options.add_argument('--disabled-extensions')




def getCaptcha():

    try:

        text = ""

        print("init process")

        #/html/body/form/div[3]/div[3]/section/div/div[4]/div/div/div[3]/div/a/i
        WebDriverWait(driver, 2)\
            .until(EC.element_to_be_clickable((By.XPATH,
            '//*[@id="FeaturedContent_LinkButton1"]')))\
            .click()

#        if path.exists("Puebla/images2/imagePuebla.png"):
#            remove('Puebla/images2/imagePuebla.png')
    

        #Esperar

        time.sleep(3)

        img_base64 = driver.execute_script("""
        var ele = arguments[0];
        var cnv = document.createElement('canvas');
        cnv.width = 215; cnv.height = 80;
        cnv.getContext('2d').drawImage(ele, 0, 0);
        return cnv.toDataURL('image/jpeg').substring(22);    
        """, driver.find_element_by_xpath("/html/body/form/div[3]/div[3]/section/div/div[4]/div/div/div[2]/div/div/img"))   #"/html/body/form/div[2]/div[3]/span[3]/div[1]/img"))
    
        print("Generate New Image")

        with open(r"images\imagePuebla.png", 'wb') as f:
        #with open(r"images/image.png", 'wb') as f:
            f.write(base64.b64decode(img_base64)) 


        pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'
        img = cv2.imread("images/imagePuebla.png")

        image_obj = Image.open("images/imagePuebla.png")
        cropped_image = image_obj.crop((0,0,160,40))
        cropped_image.save("images/imagePuebla.png")

        img = cv2.imread("images/imagePuebla.png")

        text = pytesseract.image_to_string(img)

        print("Texto de codigo")
        print(str(text.strip().replace(" ", "")))

        #driver.close()

        return str(text.strip().replace(" ", ""))

    except:

        print("error check the portal")
        #driver.close()

def get_values(placa, text):

    try:
        
        print("get values from page")
        
        # Clear input form
        driver.find_element_by_xpath('//*[@id="FeaturedContent_txtPlate"]').clear()
        driver.find_element_by_xpath('//*[@id="FeaturedContent_txtCaptcha"]').clear()

        WebDriverWait(driver, 1)\
            .until(EC.element_to_be_clickable((By.XPATH,
            '//*[@id="FeaturedContent_txtPlate"]')))\
            .send_keys(placa)          

        WebDriverWait(driver, 1)\
            .until(EC.element_to_be_clickable((By.XPATH,
            '//*[@id="FeaturedContent_txtCaptcha"]')))\
            .send_keys(str(text.strip().replace(" ", "")))  
        

        time.sleep(2)

        WebDriverWait(driver, 2)\
            .until(EC.element_to_be_clickable((By.XPATH,
            '//*[@id="FeaturedContent_btnBuscar"]')))\
            .click()

        print("Esperar a que de respuesta")

        try:

            message = WebDriverWait(driver, 5).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="FeaturedContent_lblWarningDV"]')))
            message = message.text
            #
            #print(message)

            return message

        except:
            #print("Entro en segundo message")
            
            time.sleep(2)
            try:
                 
                
                message = WebDriverWait(driver, 2).until(
                EC.presence_of_element_located((By.XPATH, '//*[@id="dv-alert-warning"]')))
                
                print("Encontrando mensaje")
                message = message.text

                fila = [placa, '', '', '', message,str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))]

                with open(PUEBLA_ADEUDOS + '.csv', "a", newline='') as fp:
                    wr = csv.writer(fp, dialect='excel')
                    wr.writerow(fila)

                fila = []
        
                print(message)
            
                return message

            except:

                #print("get some error 2")

                #return False

                try :
                    print("Get Table")

                    fila = []
                   
                    #/html/body/div[2]/section/div/form/div[3]/table[1]

                    texto_tabla = driver.find_element_by_xpath('/html/body/div[2]/section/div/form/div[3]/table[1]')
                    #//*[@id="FeaturedContent_gvDetallePagoVehiculo"]
                    #table.table:nth-child(1)
                    rows= texto_tabla.find_elements(By.TAG_NAME, 'tr')

                    #print("Numero de filas")
                    #print(len(rows))

                    #print("Valores Fila")
                    #print(fila)

                    for r in rows:
                        cols = r.find_elements(By.TAG_NAME, 'td')
                        
                        #print("Numero de columnas")
                        #print(len(cols))

                        #print(placa)
                        fila.append(placa)

                        for c in cols:
                            #print(c.text)
                            fila.append(c.text)
                        
                        fila.append(str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S')))
                        #print("Fila llenada")
                        #print(fila)

                        if len(fila) > 2 :
                            with open(PUEBLA_ADEUDOS + '.csv', "a", newline='') as fp:
                                wr = csv.writer(fp, dialect='excel')
                                wr.writerow(fila)
                       
                        fila = []   
                    
                    #print(filas)

                    return "Insert Data"

                except:

                    print("Second Table")
                    print("Get Table")

                    fila = []

                    texto_tabla = driver.find_element_by_xpath('//*[@id="FeaturedContent_gvDetallePagoVehiculo"]')
                    rows= texto_tabla.find_elements(By.TAG_NAME, 'tr')

                    for r in rows:
                        cols = r.find_elements(By.TAG_NAME, 'td')
                        
                        fila.append(placa)

                        for c in cols:
                        
                            fila.append(c.text)
                        
                        fila.append(str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S')))
                        
                        if len(fila) > 2 :
                            with open(PUEBLA_ADEUDOS + '.csv', "a", newline='') as fp:
                                wr = csv.writer(fp, dialect='excel')
                                wr.writerow(fila)
                       
                        fila = []   
                   
                    return "Insert Data"


    except:

        print("get some error")

        print(placa)
        #driver.close()

        error = "LIMPIAR INPUTS"

        return error


def reload (driver):

    print("RELOAD")
    driver.close()
    #time.sleep(10)
    driver = webdriver.Firefox(executable_path=r"C:\dfirefox\geckodriver.exe", firefox_options=options)
    driver.get("https://apps.puebla.gob.mx/PagoFacil/")

    # Configuracion segunda pantalla
    driver.set_window_position(2000, 0)
    driver.maximize_window()
    
    time.sleep(2)

    return driver


#textCaptcha = getCaptcha()


def BOT_PUEBLA():


    TEXTO_INCORRECTO = "Texto de la imagen es incorrecto."
    NO_HAY_DATOS_ASOCIADOS = "No hay datos asociados a la Placa"
    INSERT_DATA = "Insert Data"
    VEHICULO_BAJA = "VEHICULO DADO DE BAJA. FAVOR DE ACUDIR A LA OFICINA RECAUDADORA MAS CERCANA A SU DOMICILIO"
    NO_ADEUDO = "2.NO HAY ADEUDO PARA ESTA PLACA Y SERIE"
    LIMPIAR_INPUTS = "LIMPIAR INPUTS"
    value = False

    today = str(datetime.today().strftime('%Y-%m-%d'))
    PUEBLA_ADEUDOS = 'PUEBLA_ADEUDOS-' + today


    encabezados = ['PLACA', 'VALOR 1', 'VALOR 2', 'VALOR 3', 'MENSAJE', 'FECHA CONSULTA']

        
    with open(PUEBLA_ADEUDOS + '.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(encabezados)


    driver = webdriver.Firefox(executable_path=r"C:\dfirefox\geckodriver.exe", firefox_options=options)
    driver.get("https://apps.puebla.gob.mx/PagoFacil/")

    # Configuracion segunda pantalla
    driver.set_window_position(2000, 0)
    driver.maximize_window()
    time.sleep(1)

    datos=pd.read_csv('DATOS_PUEBLA.csv',header=0)

    for x in range(0,5):

        value = False

        print("Registro :" + str(x + 1) + " de " + str(len(datos)))

        print(datos['PLACA'][x])

        if (x + 1) % 5 == 0:
            #print(x)
            driver = reload(driver)

            cont = 0

            while value != True :
                
                textCaptcha = getCaptcha()

                if textCaptcha == '' :
                    textCaptcha = getCaptcha()

                value = get_values(datos['PLACA'][x], textCaptcha)

                print("El valor es: " + str(value) + "contador : " +str(cont))

                if value == LIMPIAR_INPUTS or cont > 5:
                    driver = reload(driver)
                    textCaptcha = getCaptcha()
                    value = get_values(datos['PLACA'][x], textCaptcha)

                if value == NO_HAY_DATOS_ASOCIADOS or value == INSERT_DATA or value == VEHICULO_BAJA or value == NO_ADEUDO:
                    value = True
                    driver.get("https://apps.puebla.gob.mx/PagoFacil/")
                elif value == TEXTO_INCORRECTO :
                    value = False

                #print("El valor de es : ")
                #print(value)

                cont = cont +1 

                textCaptcha= ''

        elif (x + 1) % 100 == 0:
            
            print("Descansar el sitio por 3 minutos")
            time.sleep(180)
            driver = reload(driver)
        else:
            time.sleep(2)

            cont = 0

            while value != True :
                textCaptcha = getCaptcha()

                if textCaptcha == '' :
                    textCaptcha = getCaptcha()
                
                value = get_values(datos['PLACA'][x], textCaptcha)
                
                print("El valor es: " + str(value) + "contador : " +str(cont))

                if value == LIMPIAR_INPUTS or cont > 5:
                    driver = reload(driver)
                    textCaptcha = getCaptcha()
                    value = get_values(datos['PLACA'][x], textCaptcha)


                if value == NO_HAY_DATOS_ASOCIADOS or value == INSERT_DATA or value == VEHICULO_BAJA or value == NO_ADEUDO:
                    value = True
                    driver.get("https://apps.puebla.gob.mx/PagoFacil/")
                elif value == TEXTO_INCORRECTO :
                    value = False

                cont = cont + 1


                #print("El valor de es : ")
                #print(value)

                textCaptcha= ''
                #time.sleep(4)
            
        #value = False
        #print(value)
    driver.close()