from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import pandas as pd
import time
import datetime 
import csv
from datetime import datetime
import base64
import cv2
import pytesseract


def init_process(placa):

    try:


        img_base64 = driver.execute_script("""
        var ele = arguments[0];
        var cnv = document.createElement('canvas');
        cnv.width = 150; cnv.height = 100;
        cnv.getContext('2d').drawImage(ele, 0, 0);
        return cnv.toDataURL('image/jpeg').substring(22);    
        """, driver.find_element_by_xpath('//*[@id="captcha"]')) 

        with open(r"imgRepuve.png", 'wb') as f:
            f.write(base64.b64decode(img_base64)) 


        originalImage = cv2.imread('imgRepuve.png')
        grayImage = cv2.cvtColor(originalImage, cv2.COLOR_BGR2GRAY)

        (thresh, blackAndWhiteImage) = cv2.threshold(grayImage, 225, 250, cv2.THRESH_BINARY)

        cv2.waitKey(0)
        cv2.imwrite('imgrepuvecaptchaclean.png', blackAndWhiteImage)

        pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'

        img = cv2.imread('imgrepuvecaptchaclean.png')
        text = pytesseract.image_to_string(img,config="--oem 3 --psm 4")

        driver.find_element_by_xpath('//*[@id="search_field"]').clear()

        WebDriverWait(driver, 1)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '//*[@id="search_field"]')))\
                .send_keys(placa)  

        driver.find_element_by_xpath('//*[@id="code"]').clear()

        WebDriverWait(driver, 1)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '//*[@id="code"]')))\
                .send_keys(str(text.strip().replace(" ", "")))  
        
        WebDriverWait(driver, 2)\
                .until(EC.element_to_be_clickable((By.CSS_SELECTOR,
                                                '.submit-button')))\
                .click()

        return True
    
    except:

        return False


def get_values():

    try:
        filas = []
        fila = []
    
        texto_tabla = driver.find_element_by_css_selector('table.table:nth-child(1)')
        #table.table:nth-child(1)
        rows= texto_tabla.find_elements(By.TAG_NAME, 'tr')

        for r in rows:
            cols = r.find_elements(By.TAG_NAME, 'td')
            
            for c in cols:
                fila.append(c.text)

            if len(fila) > 1:
                filas.append(fila[1])
                
            fila = []
        
        texto_tabla = driver.find_element_by_css_selector('table.table:nth-child(5)')
        #table.table:nth-child(5)
        rows = texto_tabla.find_elements(By.TAG_NAME, 'tr')
        
        for r in rows:
            cols = r.find_elements(By.TAG_NAME, 'td')
            
            for c in cols:
                fila.append(c.text)
            
            if len(fila) > 1:
                filas.append(fila[1])
            
            fila = []
    
        #print("get values completed")

        return filas 
    
    except:

        try:
            message = driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div[2]/div[2]/form/div[3]/div/span')
            message = message.text

            #print(message) 

            if message == '' :
                message = driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div[2]/div[2]/form/div[2]/div/span')
                message = message.text

                #print("No 1st error")
                #print("Danger class")
            
            else :

                #print("Placa No encontrada")
                message = driver.find_element_by_id('form-error-msj-submit')
                message = message.text

        except:
            message = 'No encontrado'
                                                
        return message


def BOT_REPUVE():
    encabezados = ['PLACA', 'ESTATUS', 'MARCA', 'MODELO', 
                    'AÑO MODELO', 'CLASE', 'TIPO', 'NÚMERO DE IDENTIFICACIÓN VEHICULAR (NIV)',
                    'NÚMERO DE CONSTANCIA DE INSCRIPCION (NCI)', 'PLACA', 'NÚMERO DE PUERTAS',
                    'PAÍS DE ORIGEN', 'VERSIÓN', 'DESPLAZAMIENTO (CC/L)', 'NÚMERO DE CILINDROS',
                    'NÚMERO DE EJES', 'PLANTA DE ENSAMBLE', 'DATOS COMPLEMENTARIOS', 'INSTITUCIÓN QUE LO INSCRIBIÓ',
                    'FECHA DE INSCRIPCIÓN', 'HORA DE ISNCRIPCIÓN', 'ENTIDAD QUE EMPLACO', 'FECHA DE EMPLACADO',
                    'FECHA DE ÚLTIMA ACTUALIZACIÓN', 'FOLIO DE CONSTANCIA DE INSCRIPCIÓN', 'OBSERVACIONES',
                    'ESTATUS','FECHA DE CONSULTA'                
                ]

    with open('RepuveBot.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(encabezados)
        

    options = webdriver.FirefoxOptions()
    options.add_argument('--start-maximized')
    options.add_argument('--disabled-extensions')



    driver = webdriver.Firefox(executable_path=r"C:\dfirefox\geckodriver.exe", firefox_options=options)
    driver.get("https://www.repuve-consultar.com/consulta-ciudadana")

    # Configuracion segunda pantalla
    driver.set_window_position(2000, 0)
    driver.maximize_window()

    #time.sleep(2)


    datos=pd.read_csv('BASE TOTAL.csv',header=0)

    for x in range(0,len(datos)):
        
        if (x + 1) % 5 == 0:
            #print(x)
            try:
                print("refresh driver")
                time.sleep(2)
                driver.get("https://www.repuve-consultar.com/consulta-ciudadana")
                #driver.refresh()
                #Servicio momentáneamente fuera de servicio. Intente en unos minutos
                #html body div.container div.row div.col-sm-12 div.box div.text form#consulta.form-horizontal div.form-group.no-bottom.buttom-holder.submitter div.col-sm-12 div.controls a.reload-button
                #/html/body/div[1]/div[2]/div/div[2]/div[2]/form/div[5]/div/div/a
                #time.sleep(2)
            except:

                print("refresh except driver")
                time.sleep(15)
                driver.get("https://www.repuve-consultar.com/consulta-ciudadana")
                #driver.refresh()

        else:
            time.sleep(2)

        placa = datos['VIN'][x]

        try :

            placa = datos['VIN'][x]
            print(x)

            init_process(placa)


            data = get_values()

            print(data)
            
            if isinstance(data, list) :
                #print(placa)
                #print("Placa a guardar")
                #print(data)
                #print(datos['PLACA'][x])

                data.append('Exitoso')
                data.append(str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S')))
                
                with open('RepuveBot.csv', "a", newline='') as fp:
                    wr = csv.writer(fp, dialect='excel')
                    wr.writerow(data)

                WebDriverWait(driver, 2)\
                        .until(EC.element_to_be_clickable((By.CSS_SELECTOR,
                                                        '.button')))\
                        .click()

                
            else :

                print(placa)

                #print(data)

                if data == 'Captcha inválido' or data == 'Formato es inválido' or data == 'Campo requerido' or data == '' or data == 'Código ingresado es incorrecto. Ingreselo nuevamente':
                    
                    WebDriverWait(driver, 1)\
                        .until(EC.element_to_be_clickable((By.ID,
                                                        'refreshCaptcha')))\
                        .click()

                    for y in range(0,5):

                        print(placa)

                        time.sleep(1)

                        init_process(placa)
                                            
                        data = get_values()
                        
                        if isinstance(data, list) :

                            data.append('Exitoso')
                            data.append(str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S')))

                            with open('RepuveBot.csv', "a", newline='') as fp:
                                wr = csv.writer(fp, dialect='excel')
                                wr.writerow(data)

                            WebDriverWait(driver, 1)\
                                .until(EC.element_to_be_clickable((By.CSS_SELECTOR,
                                                                '.button')))\
                                .click()

                            break

                        else :

                            
                            WebDriverWait(driver, 1)\
                                .until(EC.element_to_be_clickable((By.ID,
                                                                'refreshCaptcha')))\
                                .click()


                            #print('Second else')

                            if data != 'Captcha inválido' and data != 'Formato es inválido' and data != 'Campo requerido' and data != '' and data != 'Código ingresado es incorrecto. Ingreselo nuevamente':
                            
                            #print('!= message')

                                newdata = [placa,'','','','','','','','','',
                                        '','','','','','','','','','',
                                        '','','','','','',data,str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))
                                        ]

                                with open('RepuveBot.csv', "a", newline='') as fp:
                                    wr = csv.writer(fp, dialect='excel')
                                    wr.writerow(newdata)

                                break
                    
                            
                            if y == 4 :

                                #print("failed try")

                                newdata = [placa,'','','','','','','','','',
                                    '','','','','','','','','','',
                                    '','','','','','','No Exitoso',str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))
                                    ]

                                with open('RepuveBot.csv', "a", newline='') as fp:
                                    wr = csv.writer(fp, dialect='excel')
                                    wr.writerow(newdata)

                                break


                else :

                    #print("error placa")

                    newdata = [placa,'','','','','','','','','',
                            '','','','','','','','','','',
                            '','','','','','',data, str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))
                            ]

                    with open('RepuveBot.csv', "a", newline='') as fp:
                        wr = csv.writer(fp, dialect='excel')
                        wr.writerow(newdata)


            time.sleep(1)    

        except:

            print('Site unfixed')

            newdata = [placa,'','','','','','','','','',
                    '','','','','','','','','','',
                    '','','','','','Error al consultar (Portal sin respuesta)', str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S'))
                    ]

            with open('RepuveBot.csv', "a", newline='') as fp:
                wr = csv.writer(fp, dialect='excel')
                wr.writerow(newdata)

            driver.get("https://www.repuve-consultar.com/consulta-ciudadana")
            #driver.close()

            #driver.refresh()

    driver.close()

        
