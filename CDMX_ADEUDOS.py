#https://data.finanzas.cdmx.gob.mx/Front_ten/ 

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import pandas as pd
import time
import datetime 
import csv
import os.path
from datetime import date
from datetime import datetime
import shutil

def BOT_CDMX_ADEUDOS() :
    print('...')


datos=pd.read_csv('CDMX_Adeudos-2022-02-15.csv',na_filter= False ,header=0)

#Placa,Tenencia,Actualizacion Tenencia,Recargo Tenencia,Total Tenencia,Derechos,
# Actualizacion Derechos,Recargo Derechos,Total Derechos,Fecha Consulta

today = str(datetime.today().strftime('%Y-%m-%d'))
REGISTROS_Adeudos = 'REGISTROS_Adeudos-' + today


#with open(REGISTROS_Adeudos+'.csv', 'w', newline='') as file:
#    writer = csv.writer(file)
#    writer.writerow(["PLACA", "TENENCIA", "ACTUALIZACION TENENCIA","RECARGO TENENCIA","TOTAL TENENCIA",
#    "DERECHOS","ACTUALIZACION DERECHOS","RECARGO DERECHOS","TOTAL DERECHOS", "AÑO", "FECHA CONSULTA"])        


options = webdriver.FirefoxOptions()
options.add_argument('--start-maximized')
options.add_argument('--disabled-extensions')

#driver = webdriver.Firefox(executable_path=r"C:\dfirefox\geckodriver.exe", firefox_options=options)
driver = webdriver.Firefox(executable_path=r"C:\dfirefox\geckodriver.exe", firefox_options=options)

driver.get("https://data.finanzas.cdmx.gob.mx/Front_ten/")

driver.set_window_position(2000,0)
driver.maximize_window()
time.sleep(1)


filas = []
fila = []
mylist = ['','','','','','','','','','','']

limit = len(datos)
print(limit)

for x in range(0,limit):

    adeudos = datos['ADEUDOS'][x]
    placa = str(datos['PLACA'][x])
    
    print(x) 
    print (placa)
    print(adeudos)

    print("Registro : " + str(x) + " de " + str(limit))

    #time.sleep(4)

    if adeudos != '' and adeudos != 'Sin adeudos' :

            WebDriverWait(driver, 4)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '//*[@id="placa"]')))\
                .send_keys(placa)


            WebDriverWait(driver, 4)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '//*[@id="ejercicio"]')))\
                .click()


            if adeudos == 2022 :
                
                mylist[9] = 2022
                
                WebDriverWait(driver, 5)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '/html/body/div[1]/div[1]/div/div/div[2]/form/div[2]/div[2]/select/option[1]')))\
                .click()

            elif adeudos == 2021 :
                
                mylist[9] = 2021
                
                WebDriverWait(driver, 5)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '/html/body/div[1]/div[1]/div/div/div[2]/form/div[2]/div[2]/select/option[2]')))\
                .click()
            elif adeudos == 2020:

                mylist[9] = 2020

                WebDriverWait(driver, 5)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '/html/body/div[1]/div[1]/div/div/div[2]/form/div[2]/div[2]/select/option[3]')))\
                .click()
            elif adeudos == 2019:

                mylist[9] = 2019

                WebDriverWait(driver, 5)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '/html/body/div[1]/div[1]/div/div/div[2]/form/div[2]/div[2]/select/option[4]')))\
                .click()
            elif adeudos == 2018:

                mylist[9] = 2018

                WebDriverWait(driver, 5)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '/html/body/div[1]/div[1]/div/div/div[2]/form/div[2]/div[2]/select/option[5]')))\
                .click()
            elif adeudos == 2017:

                mylist[9] = 2017

                WebDriverWait(driver, 5)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '/html/body/div[1]/div[1]/div/div/div[2]/form/div[2]/div[2]/select/option[6]')))\
                .click()
            elif adeudos == 2016:

                mylist[9] = 2016

                WebDriverWait(driver, 5)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '/html/body/div[1]/div[1]/div/div/div[2]/form/div[2]/div[2]/select/option[7]')))\
                .click()
            elif adeudos == 2015:

                mylist[9] = 2015

                WebDriverWait(driver, 5)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '/html/body/div[1]/div[1]/div/div/div[2]/form/div[2]/div[2]/select/option[8]')))\
                .click()
            elif adeudos == 2014:

                mylist[9] = 2014

                WebDriverWait(driver, 5)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '/html/body/div[1]/div[1]/div/div/div[2]/form/div[2]/div[2]/select/option[9]')))\
                .click()
            elif adeudos == 2013:

                mylist[9] = 2013
                
                WebDriverWait(driver, 5)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '/html/body/div[1]/div[1]/div/div/div[2]/form/div[2]/div[2]/select/option[10]')))\
                .click()

            time.sleep(38)

            table1_text = driver.find_element_by_xpath('/html/body/div[1]/div[1]/div/section/div[3]/div/table[1]')

            rows= table1_text.find_elements(By.TAG_NAME, 'tr')
        
            fila.append(placa)

            for r in rows:
                
                cols = r.find_elements(By.TAG_NAME, 'td')
                for c in cols:
                    fila.append(c.text)

        
                print(fila)
        
            table2_text = driver.find_element_by_xpath('/html/body/div[1]/div[1]/div/section/div[3]/div/table[2]')

            rows2 = table2_text.find_elements(By.TAG_NAME, 'tr')
        
            
            for r in rows2:
                
                cols = r.find_elements(By.TAG_NAME, 'td')
                for c in cols:
                    fila.append(c.text)

            fila.append(mylist[9])
            fila.append(str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S')))

            with open(REGISTROS_Adeudos+'.csv', "a", newline='') as fp:
                wr = csv.writer(fp, dialect='excel')
                wr.writerow(fila)

            print('Fila')
            print(fila)

            fila = []
            
        
            WebDriverWait(driver, 1)\
                .until(EC.element_to_be_clickable((By.XPATH,
                                                '/html/body/div[1]/div[1]/div/section/a')))\
                .click()

    