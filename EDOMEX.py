#https://data.finanzas.cdmx.gob.mx/Front_ten/ 

from ast import Try
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import pandas as pd
import time
import datetime 
import csv
import os.path
from datetime import date
from datetime import datetime
import shutil
import requests, time

from CAPTCHA_EDOMEX import SOLVER_EDOMEX


API_KEY = "760ca4752f8f70d2709feef3a13d54d9"
#data_sitekey = '6LdF3BoUAAAAAIicTjc0LAaUefwcHM7iwf2r-SRa'
data_sitekey = '6LcXN2MUAAAAAE9ARo0U2r_9g7QQ00McEOqTuxsr'
#
page_url ='https://tenencia.edomex.gob.mx/TenenciaIndividual/tenencia/A06E1A88B8A6ED4B#/'

#"https://www.google.com/recaptcha/api2/anchor?ar=1&k=6LcXN2MUAAAAAE9ARo0U2r_9g7QQ00McEOqTuxsr&co=aHR0cHM6Ly90ZW5lbmNpYS5lZG9tZXguZ29iLm14OjQ0Mw..&hl=es-419&v=2uoiJ4hP3NUoP9v_eBNfU6CR&size=normal&cb=7r8oqk6kowjx"


def BOT_EDOMEX() :
    print('...')


today = str(datetime.today().strftime('%Y-%m-%d'))
NOMBRE_DOCUMENTO = 'EDOMEX-' + today


with open(NOMBRE_DOCUMENTO+'.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["PLACA", "MENSAJE", "TOTAL", "FECHA CONSULTA"])        

datos=pd.read_csv('EdoMex.csv',na_filter= False ,header=0)

options = webdriver.FirefoxOptions()
options.add_argument('--start-maximized')
options.add_argument('--disabled-extensions')

driver = webdriver.Firefox(executable_path=r"C:\dfirefox\geckodriver.exe", firefox_options=options)

time.sleep(5)

driver.set_window_position(2000,0)
driver.maximize_window()
time.sleep(1)

driver.get(page_url)

for x in range(0,len(datos)):

    placa = str(datos['Placa'][x])

    WebDriverWait(driver, 4)\
        .until(EC.element_to_be_clickable((By.XPATH,
        '/html/body/div[3]/div/div[3]/div[1]/div/div/div[1]/form/table/tr[2]/td[1]/input')))\
        .send_keys(placa)

    SOLVER_EDOMEX(driver,data_sitekey,page_url,API_KEY)

    WebDriverWait(driver, 3)\
        .until(EC.element_to_be_clickable((By.XPATH,
        '/html/body/div[3]/div/div[3]/div[1]/div/div/div[1]/form/table/tr[3]/td/button')))\
        .click()
    
    try:
        time.sleep(4)
        message = driver.find_element_by_xpath("/html/body/div[3]/div/div[3]/div[8]/div[2]/table/tr[1]/td/label").text

        WebDriverWait(driver, 3)\
            .until(EC.element_to_be_clickable((By.XPATH,
            '/html/body/div[3]/div/div[3]/div[8]/div[2]/table/tr[2]/td/button')))\
            .click()


    except :
        #time.sleep(10)
        #driver.close()
        message = ""
                
    total = driver.find_element_by_xpath("/html/body/div[3]/div/div[3]/div[5]/div/div/div/div[2]/div/div/div/table/tfoot/tr/td[7]").text
    
    fila = []
    fila.append(placa)
    fila.append(message)
    fila.append(total)
    fila.append(str(datetime.today().strftime('%Y-%m-%d-%H:%M:%S')))

    with open(NOMBRE_DOCUMENTO+'.csv', "a", newline='') as fp:
        wr = csv.writer(fp, dialect='excel')
        wr.writerow(fila)

    time.sleep(3)
    WebDriverWait(driver, 3)\
        .until(EC.element_to_be_clickable((By.XPATH,
        '/html/body/div[3]/div/div[3]/div[7]/div/div/div/div[2]/div/div[2]/div/button')))\
        .click()

driver.close()